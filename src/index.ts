/*******************************************************************************
 * @copyright 2022 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import { Plugin as IPlugin } from 'vue';

import ThrowError from './components/ThrowError.vue';

export const Plugin: IPlugin = {
  install(app): void {
    app.component('ThrowError', ThrowError);
  },
};

export { ThrowError as Component };
