/*******************************************************************************
 * @copyright 2022 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import { createApp } from 'vue';

import App from './App.vue';

createApp(App).mount('#app');
