/*******************************************************************************
 * @copyright 2022 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import { ComponentOptions } from 'vue';
import { describe, expect, it } from 'vitest';
import { mount } from '@vue/test-utils';

import ThrowError, { DEFAULT_ERROR_MESSAGE } from './ThrowError.vue';

const BASE_COMPONENT_OPTIONS: ComponentOptions = {
  components: { ThrowError },

  // Needed for the `@vue/test-utils` to work correctly. Without this declaration, only the very 1st test case
  // will succeed, and every subsequent will fail (regardless of its contents).
  errorCaptured: () => false,
};

describe('ThrowError', () => {
  it('throws a default error when mounted', () => {
    try {
      mount({
        ...BASE_COMPONENT_OPTIONS,
        template: `<ThrowError />`,
      });
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      expect(error).toHaveProperty('message');
      expect((error as Error).message).toContain(DEFAULT_ERROR_MESSAGE);
    }
  });

  it('throws a custom error when mounted', () => {
    const CUSTOM_ERROR_MESSAGE: string = 'Donec sed odio dui.';

    try {
      mount({
        ...BASE_COMPONENT_OPTIONS,
        template: `<ThrowError>${ CUSTOM_ERROR_MESSAGE }</ThrowError>`,
      });
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      expect(error).toHaveProperty('message');
      expect((error as Error).message).toContain(CUSTOM_ERROR_MESSAGE);
    }
  });
});
