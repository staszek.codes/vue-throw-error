import { defineComponent as a, openBlock as _, createElementBlock as c, createElementVNode as n, renderSlot as d, createTextVNode as p, toDisplayString as i, pushScopeId as E, popScopeId as l } from "vue";
const h = "I shouldn't be rendered! If I did, it probably means that you did something wrong (e.g. forgot to populate a slot).", m = a({
  name: "ThrowError",
  data: () => ({ DEFAULT_ERROR_MESSAGE: h }),
  computed: {
    errorMessage() {
      var o;
      const e = this.$refs.body;
      return e instanceof HTMLParagraphElement ? (o = e.textContent) != null ? o : this.DEFAULT_ERROR_MESSAGE : this.DEFAULT_ERROR_MESSAGE;
    }
  },
  mounted() {
    throw new Error(this.errorMessage);
  }
});
const u = (e, o) => {
  const t = e.__vccOpts || e;
  for (const [r, s] of o)
    t[r] = s;
  return t;
}, f = (e) => (E("data-v-92696135"), e = e(), l(), e), g = { class: "error-message" }, R = /* @__PURE__ */ f(() => /* @__PURE__ */ n("h2", { class: "error-message__heading" }, "Oops!", -1)), S = {
  class: "error-message__body",
  ref: "body"
};
function y(e, o, t, r, s, w) {
  return _(), c("section", g, [
    R,
    n("p", S, [
      d(e.$slots, "default", {}, () => [
        p(i(e.DEFAULT_ERROR_MESSAGE), 1)
      ], !0)
    ], 512)
  ]);
}
const T = /* @__PURE__ */ u(m, [["render", y], ["__scopeId", "data-v-92696135"]]), b = {
  install(e) {
    e.component("ThrowError", T);
  }
};
export {
  T as Component,
  b as Plugin
};
