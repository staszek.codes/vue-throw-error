# vue-throw-error

![](https://img.shields.io/npm/v/@staszek998/vue-throw-error?color=%2314ffec)
![](https://img.shields.io/npm/l/@staszek998/vue-throw-error?color=%2314ffec)

> A simple _Vue_ component which throws an error when it gets mounted and also prints some useful
> info on the screen, where the component was originally placed (within the DOM).

**Table of contents**

[[_TOC_]]

## Why bother?

Often, when designing reusable components, we put slots in them, that should be populated by the
parent component. If we don't provide the content for a given **required** slot, our component
will be broken. In such cases, the following `<ThrowError>` component may turn out to be useful, and
will throw an exception if it gets rendered (for more explanation, see the "Usage" section below).

## Installation

### 1. Download the package

#### 1.A. Using NPM.

```bash
npm install --save @staszek998/vue-throw-error
```

#### 1.B. Using Yarn.

```bash
yarn add @staszek998/vue-throw-error
```

### 2. Implement the component within your _Vue_ application

_vue-throw-error_ exposes two named exports: `Component` and `Plugin`. Here's how to use them:

#### 2.A. Using the `Component` export

##### 2.A.a. Installing the component globally, for use _within the whole app_

```typescript
// main.(j|t)s

import { createApp } from 'vue'
import { Component as ThrowError } from 'vue-throw-error'

const app = createApp({})

app.component('ThrowError', ThrowError)
```

##### 2.A.b. Installing the component locally, for use _within a single component_

```vue
<!-- MyComponent.vue -->

<script>
import { Component as ThrowError } from 'vue-throw-error'

export default {
  components: { ThrowError }
}
</script>
```

#### 2.B. Using the `Plugin` export

```typescript
// main.(j|t)s

import { createApp } from 'vue'
import { Plugin as ThrowError } from 'vue-throw-error'

const app = createApp({})

app.use(ThrowError)
```

## Usage

### Default

You can utilize the `<ThrowError>` component without any further configuration:

```vue
<!-- MyComponent.vue -->

<template>
  <div>
    <slot>
      <!--
        This slot is required and HAS TO be populated for the component to work correctly.
        If you don't fill this slot, an error will be thrown.
      -->
      <ThrowError />
    </slot>
  </div>
</template>
```

```vue
<!-- App.vue -->

<template>
  <MyComponent />
</template>

<script>
import MyComponent from 'MyComponent.vue'

export default {
  components: { MyComponent }
}
</script>
```

This will produce the following output:

- browser:
  ![](./screenshots/default-browser.png)
- console:
  ![](./screenshots/default-console.png)

### Customized

However, you can customize the error message:

```vue
<!-- MyComponent.vue -->

<template>
  <div>
    <slot>
      <!--
        This slot is required and HAS TO be populated for the component to work correctly.
        If you don't fill this slot, an error will be thrown.
      -->
      <ThrowError>Maecenas faucibus mollis interdum.</ThrowError>
    </slot>
  </div>
</template>
```

```vue
<!-- App.vue -->

<template>
  <MyComponent />
</template>

<script>
import MyComponent from 'MyComponent.vue'

export default {
  components: { MyComponent }
}
</script>
```

This will produce the following output:

- browser:
  ![](./screenshots/custom-browser.png)
- console:
  ![](./screenshots/custom-console.png)

## Styling

In order to receive the base styles for the `<ThrowError>` component, you should import the exported
CSS file, using one of the following options:

### A. Inside your main stylesheet (recommended)

```scss
// main.scss

@import 'vue-throw-error/dist/style.css';
```

### B. Inside your root component

#### B.a. inside the `<style>` section

```vue
<!-- App.vue -->

<style lang="scss">
@import 'vue-throw-error/dist/style.css';
</style>
```

#### B.b. inside the `<script>` section

```vue
<!-- App.vue -->

<script>
import 'vue-throw-error/dist/style.css';
</script>
```

### C. Inside your main entry file

```typescript
// main.(j|t)s

import 'vue-throw-error/dist/style.css';
```

### Style overrides

The styling of the `<ThrowError>` component is limited to the absolute minimum.
Both `font-family` and `font-size` should be automatically adjusted to the ones being used within
your application.

## Authors

- IDEALIGN Stanisław Gregor
  [📧](mailto:developer@staszek.codes) [🌐](https://gitlab.com/staszek.codes)

---

Copyright © 2022 IDEALIGN Stanisław Gregor

Licensed under the [MIT license](./LICENSE).
